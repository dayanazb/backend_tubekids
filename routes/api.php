<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'UserController@index');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});

Route::post('register', 'RegisterController@create');

Route::post('send-mail', 'MailController@sendMail')->name('mail');

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::post('video', 'VideoController@store');
Route::get('lista_videos', 'VideoController@show');
Route::post('search', 'VideoController@searchVideo');
Route::post('/update/{id}', 'VideoController@update');
Route::delete('/delete/{id}', 'VideoController@destroy');

Route::post('perfil', 'PerfilController@store');
Route::post('lista_perfiles', 'PerfilController@index');
Route::put('/perfil', 'PerfilController@update');
Route::delete('/perfil', 'PerfilController@destroy');
Route::get('/perfil/{id}', 'PerfilController@show');

Route::put('/perfil/{id}/delete', 'PerfilController@destroy');


Route::post('video', 'VideoController@store');
Route::post('lista_videos', 'VideoController@index');
Route::put('/video', 'VideoController@update');
Route::delete('/video', 'VideoController@destroy');
Route::get('/video/{id}', 'VideoController@show');

Route::put('/video/{id}/delete', 'VideoController@destroy');

Route::post('sms', 'SmsController@sendSms');

