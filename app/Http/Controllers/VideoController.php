<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PayUService\Exception;
use App\Video;
use DB;

class VideoController extends Controller
{
    public function validateUser($token){
        return auth()->setToken($token)->user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($this->validateUser($request['token'])){
        $videos = Video::all();
        return response()->json(['videos' => $videos, 'code' => 200]);
        }else{
            return response()->json([ 'message' => 'No hay videos', 'code' => 401]);
        } 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // recibimos los parámetros
        if($this->validateUser($request['token'])){
            $playlist = '1';
            $video = Video::create([
                'name' => $request['name'],
                'path' => $request['path'],
                'id_playlist' => $playlist,
            ]);     
            return response()->json([ 'video' => $video, 'code' => '201']);
        }else{
            return response()->json([ 'message' => 'Usuario no autorizado', 'code' => 401]);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::findOrFail($id);
        return response()->json(['video' => $video, 'code' => 200]);
    }


    public function searchVideo(Request $request){
         $q = $request['name'];
        $video = Video::where ( 'name', 'LIKE', '%' . $q . '%' )->get ();
        if (count ( $video ) > 0){
            return response()->json(['video' => $video, 'code' => '201']);
        } else{
            return  response()->json(['message' => 'No Details found. Try to search again !' ,'code' => '201']); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($this->validateUser($request['token'])){
            $video = Video::findOrFail($request['id']);

            $video ->name = $request ->name;
            $video ->path = $request ->path;
            $video -> save();
            return response()->json(['video' => $video, 'code' => 200]);
        }else{
            return response()->json([ 'message' => 'Usuario no autorizado', 'code' => 401]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($this->validateUser($request['token'])){
                $video = Video::findOrFail($request['id']);
                $video->delete();
            return response()->json(['code' => 200]);
            }else{
                return response()->json([ 'message' => 'Usuario no autorizado', 'code' => 401]);
            } 
        }catch (Exception $e){
            return response()->json(['message' => 'Fatal Error', 'code' => '404']);

        }
    }
}
