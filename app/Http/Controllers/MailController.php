<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendgridMail;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class MailController extends Controller
{
    use SendGrid;
    public function sendMail(){
        $data = ['message' => 'This is a test!'];
        Mail::to('andres9366ha@gmail.com')->send(new SendgridMail($data));
    }


}
