<?php

namespace App\Http\Controllers;
require 'vendor/autoload.php';

use Illuminate\Http\Request;

class SmsController extends Controller
{
	    // Comment out the above line if not using Composer
	// require("<PATH TO>/sendgrid-php.php");
	// If not using Composer, uncomment the above line and
	// download sendgrid-php.zip from the latest release here,
	// replacing <PATH TO> with the path to the sendgrid-php.php file,
	// which is included in the download:
	// https://github.com/sendgrid/sendgrid-php/releases
	public function sendSms(){
		$email = new \SendGrid\Mail\Mail(); 
		$email->setFrom("da170613@gmail.com", "Dayana");
		$email->setSubject("Sending with SendGrid is Fun");
		$email->addTo("test@example.com", "TubeKids");
		$email->addContent("text/plain", "and easy to do anywhere, even with PHP");
		$email->addContent(
		    "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
		);
		$sendgrid = new \SendGrid(getenv('AC730693d1326e264b40a6129821e69712'));
		try {
		    $response = $sendgrid->send($email);
		    print $response->statusCode() . "\n";
		    print_r($response->headers());
		    print $response->body() . "\n";
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		}
	}

}
