<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Services\PayUService\Exception;
use JWTFactory;
use JWTAuth;
use Response;
use DB;
//require 'vendor/autoload.php';

class RegisterController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'country' => [''],
            'birth_date' => ['required', 'string', 'max:255'], /// buscar la validacion para fecha
            'phone' => ['required', 'integer', 'min:8'],
           
        ]);
    }


    function esMayor($data){
        
        /*$fecha = new DateTime($data);
        $fechaActual = new DateTime("now");
        $mayor =  $fecha->diff($fechaActual);
        if ($mayor->format("%y") >= 18) {
            return false;
        } else {*/
            return true;

        
        //dd('true');
        // list($ano,$mes,$dia) = explode("-",$data); 
        // $ano_diferencia  = date("Y") - $ano;
        // $mes_diferencia = date("m") - $mes;
        // $dia_diferencia   = date("d") - $dia;
        // //dd($dia_diferencia >= 0 && $mes_diferencia >= 0 && $ano_diferencia >= 18);
        // return ($dia_diferencia >= 0 && $mes_diferencia >= 0 && $ano_diferencia >= 18);

      }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {  

       try {
           $user = DB::table('users')->where('email', $request['email'])->first();
            if(!$user){
                if($this->esMayor($request['birth_date'])) {
                    $user = User::create([
                         'name' => $request['name'],
                         'last_name' => $request['last_name'],
                         'email' => $request['email'],
                         'password' => Hash::make($request['password']),
                         'country' => $request['country'],
                         'birth_date' => $request['birth_date'],
                         'phone' => $request['phone']
                    ]);
                                  
                    $email = new \SendGrid\Mail\Mail(); 
                    $email->setFrom("dazamora@est.utn.ac.cr", "Example User");
                    $email->setSubject("Confirma tu registro a Tubekids!");
                    $email->addTo($user->email, "Example User");
                    $email->addContent(
                        "text/html", "<h2>Hola {{ $user->name }}, gracias por registrarte en <strong>Tubekids</strong></h2>
                        <p>Por favor confirma tu correo electrónico en SendGrid</p>
                        <p>Para ello simplemente debes hacer click en el siguiente enlace:</p>"
                      //  <a href= " {{ url('/register/verify/'. {$user->$confirmation_code})}}"> Clic para confirmar tu email</a>
                    );
                    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                    $response = $sendgrid->send($email);
                    try {
                        return response()->json(['user' => $user, 'message' => 'mensage enviado', 'code' => 200]);
                    } catch (Exception $e) {
                        echo 'Caught exception: '. $e->getMessage() ."\n";
                    }
     
                    return response()->json(['user' => $user, 'message' => 'Usuario creado exitosamente', 'code' => 201]);
                }else{
                     return response()->json(['code' => 400, 'message' => 'No se pudo crear usuario.']);
                 } 
            }else{
                return response()->json(['message' => 'Usuario existente', 'code' => 400]);
            }   
       }catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => 400]);
       }
    }

}
