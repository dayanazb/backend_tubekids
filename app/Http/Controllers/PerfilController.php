<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfil;
use App\User;

class PerfilController extends Controller
{

    public function validateUser($token){
        return auth()->setToken($token)->user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($this->validateUser($request['token'])){
        $perfiles = Perfil::all();
        return response()->json(['perfiles' => $perfiles, 'code' => 200]);
        }else{
            return response()->json([ 'message' => 'No hay perfiles', 'code' => 401]);
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->validateUser($request['token'])){
            $perfil = Perfil::create([
                'name' => $request['name'],
                'username' => $request['username'],
                'pin' => $request['pin'],
                'edad' => $request['edad'],
            ]);
         return response()->json([ 'perfil' => $perfil, 'code' => 201]);
        }else{
            return response()->json([ 'message' => 'Usuario no autorizado', 'code' => 401]);
        }               
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $perfil = Perfil::findOrFail($id);
        return response()->json(['perfil' => $perfil, 'code' => 200]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($this->validateUser($request['token'])){
            $perfil = Perfil::findOrFail($request['id']);

            $perfil ->name = $request ->name;
            $perfil ->username = $request ->username;
            $perfil ->pin = $request ->pin;
            $perfil ->edad = $request ->edad;
            $perfil -> save();
            return response()->json(['perfil' => $perfil, 'code' => 200]);
        }else{
            return response()->json([ 'message' => 'Usuario no autorizado', 'code' => 401]);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($this->validateUser($request['token'])){
                $perfil = Perfil::findOrFail($request['id']);
                $perfil->delete();
                return response()->json(['code' => 200]);
            }else{
                return response()->json([ 'message' => 'Usuario no autorizado', 'code' => 401]);
            } 
        }catch (Exception $e){
            return response()->json(['message' => 'Fatal Error', 'code' => '404']);

        }
    }
}
