<?php


use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::truncate();
        User::create([
            
            'name' => 'Administrator',
            'last_name' => 'Zamora',
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin'),
            'country' => 'Cq',
            'birth_date' => '12-03-2019',
            'phone' => '60096788'         
        ]);
    }
}